from django.contrib import admin

from .models import Customer, Contact


class ContactInline(admin.TabularInline):
    model = Contact


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    inlines = [ContactInline]
    list_display = ('name',)
