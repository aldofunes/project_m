from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic

from .models import Customer

class IndexView(LoginRequiredMixin, generic.ListView):
    template_name = 'customers/index.html'
    context_object_name = 'customer_list'

    def get_queryset(self):
        """Return the last five published customers."""
        return Customer.objects.order_by('-name')[:5]


class DetailView(LoginRequiredMixin, generic.DetailView):
    model = Customer
    template_name = 'customers/detail.html'

