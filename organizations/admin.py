from django.contrib import admin
from django_tenants.admin import TenantAdminMixin

from organizations.models import Organization, Domain


class DomainInline(admin.TabularInline):
    model = Domain


@admin.register(Organization)
class OrganizationAdmin(TenantAdminMixin, admin.ModelAdmin):
    inlines = [DomainInline]
    list_display = (
        'name',
        'logo_url',
        'street_address',
        'city',
        'state',
        'postal_code',
        'country_code',
        'phone_number',
        'website_url',
        'employees',
        'founded_at',
        'annual_revenue_range',
    )
