from django.db import models
from django_tenants.models import TenantMixin, DomainMixin


class Organization(TenantMixin):
    name = models.TextField()
    logo_url = models.TextField(null=True)
    street_address = models.TextField(null=True)
    city = models.TextField(null=True)
    state = models.TextField(null=True)
    postal_code = models.TextField(null=True)
    country_code = models.TextField(null=True)
    phone_number = models.TextField(null=True)
    website_url = models.TextField(null=True)
    employees = models.IntegerField(null=True)
    founded_at = models.DateField(null=True)
    annual_revenue_range = models.TextField(null=True)

    created_on = models.DateField(auto_now_add=True)

    # default true, schema will be automatically created and synced when it is saved
    auto_create_schema = True


class Domain(DomainMixin):
    pass
