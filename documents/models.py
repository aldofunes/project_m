from django.db import models

class Document(models.Model):
    name = models.TextField()
    url = models.URLField()
    created_on = models.DateField(auto_now_add=True)
