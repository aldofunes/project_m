const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  content: [
    "templates/**/*.html",
    "../customers/**/*.html",
    "../documents/**/*.html",
    "../organizations/**/*.html",
    "../templates/**/*.html",
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Inter var", ...defaultTheme.fontFamily.sans],
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require("@tailwindcss/typography"),
    require("@tailwindcss/forms"),
    require("@tailwindcss/line-clamp"),
    require("@tailwindcss/aspect-ratio"),
  ],
};
